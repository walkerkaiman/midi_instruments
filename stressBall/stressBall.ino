#include <ResponsiveAnalogRead.h>
#include <Bounce2.h>

const int DEBOUNCE_INTERVAL = 5;
const int FRAME_DELAY_MILLI = 20;
const int INPUT_VELOCITY_PIN = A0;
const int INPUT_NOTE_PIN = A1;
const int INPUT_TRIGGER_PIN = 16;

const int NOTE_READ_MIN = 0;
const int NOTE_READ_MAX = 1023;
const int VELOCITY_READ_MIN = 400; // Requires calibration.
const int VELOCITY_READ_MAX = 468; // Requires calibration.

const int MIDI_CHANNEL = 1;
const int MIDI_NOTE_MIN = 0;
const int MIDI_NOTE_MAX = 127;
const int VELOCITY_MIN = 10;
const int VELOCITY_MAX = 127;
const int MIDI_CONTROLLER_EXPRESSION_ADDRESS = 11;

Bounce noteTrigger = Bounce();
ResponsiveAnalogRead velocityRead(INPUT_VELOCITY_PIN, true);
ResponsiveAnalogRead noteRead(INPUT_NOTE_PIN, true);

int prevNote, prevVelocity = 0;

void setup() {
  noteTrigger.attach(INPUT_TRIGGER_PIN);
  noteTrigger.interval(DEBOUNCE_INTERVAL);
  pinMode(INPUT_VELOCITY_PIN, INPUT);
  pinMode(INPUT_NOTE_PIN, INPUT);
  pinMode(INPUT_TRIGGER_PIN, INPUT);
}

void loop() {
  // Update the readings of the sensors
  velocityRead.update();
  noteRead.update();
  noteTrigger.update();
  
  bool playing = noteTrigger.read() == LOW ? true : false;
  int currentNote = getNote();
  int currentVelocity = getVelocity();

  // Did the trigger state change this frame?
  if (noteTrigger.fell()) {
    usbMIDI.sendNoteOn(currentNote, VELOCITY_MAX, MIDI_CHANNEL);
  }
  else if (noteTrigger.rose()) {
    allNotesOff();
  }

  // Are we currently playing a note?
  if (playing) {
    if (currentVelocity != prevVelocity) {
      usbMIDI.sendControlChange(MIDI_CONTROLLER_EXPRESSION_ADDRESS, currentVelocity, MIDI_CHANNEL);
    }

    if (currentNote != prevNote) {
      allNotesOff();
      usbMIDI.sendNoteOn(currentNote, VELOCITY_MAX, MIDI_CHANNEL);
    }
  }
  
  prevNote = currentNote;
  prevVelocity = currentVelocity;

  delay(FRAME_DELAY_MILLI);
}

void allNotesOff() {
  for (int n = MIDI_NOTE_MIN; n < MIDI_NOTE_MAX; n++) {
    usbMIDI.sendNoteOff(n, VELOCITY_MAX, MIDI_CHANNEL);
  }
}

int getNote() {
  int currentRead = noteRead.getValue();
  int MIDI_note = int(map(currentRead, NOTE_READ_MIN, NOTE_READ_MAX, MIDI_NOTE_MIN, MIDI_NOTE_MAX));
  MIDI_note = constrain(MIDI_note, MIDI_NOTE_MIN, MIDI_NOTE_MAX);
  
  return MIDI_note;
}

int getVelocity() {
  int currentRead = velocityRead.getValue();
  int MIDI_velocity = int(map(currentRead, VELOCITY_READ_MIN, VELOCITY_READ_MAX, VELOCITY_MIN, VELOCITY_MAX));
  MIDI_velocity = VELOCITY_MAX - constrain(MIDI_velocity, VELOCITY_MIN, VELOCITY_MAX);
  
  return MIDI_velocity;
}

