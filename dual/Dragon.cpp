/*
   Dragon.cpp - Library for controlling a note set on a Dual Dragon.
   Created by Kaiman Walker, April 10, 2017.
*/

#include "Arduino.h"
#include "Dragon.h"

Dragon :: Dragon(byte ledPin, byte speedPin, byte notePin, byte buttonPin, byte midiChannel) {
  _ledPin = ledPin;
  _speedPin = speedPin;
  _notePin = notePin;
  _midiChannel = midiChannel;
  _noteVelocity = 127; // MAGIC NUMBER. Currently only plays one velocity. May make changeable in the future.
  
  _buttonState = Bounce();
  _buttonState.attach(buttonPin);
  _buttonState.interval(5); // MAGIC NUMBER. Debounce Time.

  _playing = false;

  pinMode(_ledPin, OUTPUT);
  pinMode(buttonPin, INPUT);
  pinMode(_speedPin, INPUT);
  pinMode(_notePin, INPUT);
}

/*
 * updateSpeed()
 * _currentSpeed is the amount of time that a note is played AND the amount of time that a 
 * note is not played. _currentSpeed is set by an analog read of a potentiometer on the MIDI
 * controller.
*/
void Dragon :: updateSpeed() {
  _currentSpeed = constrain(map(analogRead(_speedPin), 0, 1023, 2000, 50), 50, 2000);
}

/*
 * updateNote()
 * _currentNote is the note value to send as a MIDI device. The MIDI controller has a feature
 * to use certain scales. The analog read from the _notePin is converted to an index value
 * and then looked up in an array of notes in the currently selected scale.
*/
void Dragon :: updateNote(const byte currentScale[]) {
  unsigned int potRead = analogRead(_notePin);
  byte numberOfNotesInCurrentScale = sizeof(currentScale) / sizeof(byte);
  byte scaleIndex = byte(map(potRead, 0, 1023, 0, numberOfNotesInCurrentScale - 1));
  scaleIndex = constrain(scaleIndex, 0, numberOfNotesInCurrentScale - 1);
  
  _currentNote = currentScale[scaleIndex];
}

/*
 * notesOff()
 * All of the notes on a channel has to be turn off when the notes aren't playing. 
 * TODO (maybe): Find a way to turn off notes while the two dragons use the same MIDI channel.
*/
void Dragon :: _notesOff() {
  for (int note = 0; note < 127; note++) {
    usbMIDI.sendNoteOff(note, _noteVelocity, _midiChannel);
  }
}

void Dragon :: updateButton() {
  _buttonState.update();

  if (millis() - _noteStartTime >= _currentSpeed) {
    //Serial.println("Toggle LED state.");
    digitalWrite(_ledPin, _playing);

    _playing = ! _playing;
    _noteStartTime = millis();

    if (_buttonState.read() == HIGH) {
      if (_playing) {
        usbMIDI.sendNoteOn(_currentNote, _noteVelocity, _midiChannel);
      } else {
        _notesOff();
      }
    }
  }

  if (_buttonState.rose()) {
    //Serial.println("Button pressed this frame.");
    usbMIDI.sendNoteOn(_currentNote, _noteVelocity, _midiChannel);
    digitalWrite(_ledPin, HIGH);
    _playing = true;
    _noteStartTime = millis();
  }

  if (_buttonState.fell()) {
    //Serial.println("Button released this frame.");
    _notesOff();
    _playing = false;
  }
}

