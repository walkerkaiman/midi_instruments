/*
  Side A
  LED Pin - 10
  Speed Pin - 22
  Note Pin - 23
  MIDI Channel - 1
  Note Min - 0
  Note Max - 64
  Button Pin - 2

  Side B
  LED Pin - 11
  Speed Pin - 14
  Note Pin - 15
  MIDI Channel - 1
  Note Min - 65
  Note Max - 127
  Button Pin - 3

  Scale Select Pin - 15
*/
#include "Dragon.h"
#include "MIDI_SCALES.h"

const byte SCALE_PIN = 15;

// LED PIN, SPEED PIN, NOTE PIN, MIDI CHANNEL, BUTTON PIN
Dragon A(10, 22, 23, 2, 1);
Dragon B(11, 14, 15, 3, 2);

void setup() {
}

void loop() {
  A.updateNote(SCALES[getScaleIndex(SCALE_PIN)]);
  B.updateNote(SCALES[getScaleIndex(SCALE_PIN)]);
  
  A.updateSpeed();
  B.updateSpeed();

  A.updateButton();
  B.updateButton();
}

/* Returns a value from 0 to NUM_OF_SCALES dependent on a analog read of a selector switch.
 * Selector switch is using a resistor ladder with an analog read.
 * calibration required for analog readings.
 */
byte getScaleIndex(const byte pin) {
  unsigned int pinRead = analogRead(pin);
  
  //Serial.print(pinRead);
  return byte(constrain(map(pinRead, 0, 1023, 0, NUM_OF_SCALES), 0, NUM_OF_SCALES));
}

