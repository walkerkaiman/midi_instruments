/*
   Dragon.h - Library for controlling a note set on a Dual Dragon.
   Created by Kaiman Walker, April 10, 2017.
*/

#ifndef Dragon_h
#define Dragon_h

#include "Arduino.h"
#include <Bounce2.h>

class Dragon {
  public:
    Dragon(byte ledPin, byte speedPin, byte notePin, byte buttonPin, byte midiChannel);
    void updateNote(const byte currentScale[]);
    void updateSpeed();
    void updateButton();
  private:
    Bounce _buttonState;
    byte _ledPin;
    byte _speedPin;
    byte _notePin;
    byte _midiChannel;
    byte _noteVelocity;
    unsigned int _currentSpeed;
    byte _currentNote;
    boolean _playing;
    unsigned int _noteStartTime;
    void _notesOff();
};
#endif
