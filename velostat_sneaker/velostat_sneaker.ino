#include <ResponsiveAnalogRead.h>

const uint16_t BAUD_RATE = 115200;
const uint8_t SENSOR_PIN = A0;
const uint16_t SENSOR_READ_MIN = 400;
const uint16_t SENSOR_READ_MAX = 520;
const uint8_t FRAME_DELAY_MILLI = 50;

const uint8_t MIDI_NOTE_MIN = 20;
const uint8_t MIDI_NOTE_MAX = 100;
const uint8_t MIDI_CHANNEL = 1;
const uint8_t MIDI_VELOCITY = 127;
const uint8_t MIDI_NOTE_DELAY = 50;

ResponsiveAnalogRead sensor(SENSOR_PIN, true);

void setup() {
  Serial.begin(BAUD_RATE);
  pinMode(SENSOR_PIN, INPUT);
}

void loop() {
  sensor.update();
  uint16_t sensorRead = constrain(sensor.getValue(), SENSOR_READ_MIN, SENSOR_READ_MAX);
  
  if (sensor.hasChanged()) {
    uint8_t currentNote = map(sensorRead, SENSOR_READ_MIN, SENSOR_READ_MAX, MIDI_NOTE_MIN, MIDI_NOTE_MAX);
    usbMIDI.sendNoteOn(currentNote, MIDI_VELOCITY, MIDI_CHANNEL);
    delay(MIDI_NOTE_DELAY);
    usbMIDI.sendNoteOff(currentNote, MIDI_VELOCITY, MIDI_CHANNEL);
    delay(MIDI_NOTE_DELAY);
  }
}
