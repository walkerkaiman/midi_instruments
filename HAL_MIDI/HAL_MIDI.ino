#include <Bounce2.h>

const uint16_t BAUD_RATE = 9600;
const uint8_t DEBOUNCE_TIME = 5;

const uint8_t SPEED_POT_PIN = 23;
const uint8_t NOTE_POT_PIN = 22;
const uint8_t BUTTON_INPUT_PIN = 2;
const uint8_t BUTTON_LED_PIN = 13;

const uint8_t MIDI_CHANNEL = 4;
const uint8_t MIDI_VELOCITY = 127;

const uint16_t SPEED_MAX = 100000;
const uint8_t SPEED_MIN = 5000;
const uint8_t NOTE_MAX = 120;
const uint16_t NOTE_MIN = 20;
const uint16_t SPEED_POT_MAX = 1023;
const uint16_t SPEED_POT_MIN = 0;
const uint16_t NOTE_POT_MAX = 1023;
const uint16_t NOTE_POT_MIN = 0;

Bounce buttonState = Bounce();

int currentSpeed = 0;
int currentNote = 0;
int frameCount = 0;
boolean notePlaying = true;

void setup() {
  pinMode(SPEED_POT_PIN, INPUT);
  pinMode(NOTE_POT_PIN, INPUT);
  pinMode(BUTTON_INPUT_PIN, INPUT);
  pinMode(BUTTON_LED_PIN, OUTPUT);

  buttonState.attach(BUTTON_INPUT_PIN);
  buttonState.interval(DEBOUNCE_TIME);
}

void loop() {
  // Update the controller's state every frame.
  buttonState.update();

  currentSpeed = analogRead(SPEED_POT_PIN);
  currentSpeed = map(currentSpeed, SPEED_POT_MIN, SPEED_POT_MAX, SPEED_MAX, SPEED_MIN);
  currentSpeed = int(constrain(currentSpeed, SPEED_MIN, SPEED_MAX));

  currentNote = analogRead(NOTE_POT_PIN);
  currentNote = map(currentNote, NOTE_POT_MIN, NOTE_POT_MAX, NOTE_MIN, NOTE_MAX);
  currentNote = int(constrain(currentNote, NOTE_MIN, NOTE_MAX));

  if (buttonState.rose()) {
    //Serial.println("Button pressed this frame.");
    usbMIDI.sendNoteOn(currentNote, MIDI_VELOCITY, MIDI_CHANNEL);
    digitalWrite(BUTTON_LED_PIN, HIGH);
    notePlaying = true;
    frameCount = 0;
  }

  if (buttonState.fell()) {
    //Serial.println("Button released this frame.");
    allMIDI_off();
    notePlaying = false;
  }

  // Toggle the state of the MIDI note and button's LED when the frameCount hits the currentSpeed
  if (frameCount >= currentSpeed) {
    //Serial.println("Toggle LED state.");

    int LED_state = notePlaying == true ? LOW : HIGH;
    digitalWrite(BUTTON_LED_PIN, LED_state);

    notePlaying = ! notePlaying;
    frameCount = 0;

    if (buttonState.read() == HIGH) {
      if (notePlaying) {
        usbMIDI.sendNoteOn(currentNote, MIDI_VELOCITY, MIDI_CHANNEL);
      } else {
        allMIDI_off();
      }
    }
  }

  frameCount++;
}

void allMIDI_off(){
  for (int note = 0; note < 127; note++) {
    usbMIDI.sendNoteOff(note, MIDI_VELOCITY, MIDI_CHANNEL);
  }
}

